//require en js es como un import en java

var express = require ('express');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

// 20_03_2018 incluimos líneas para consultar a mLab
var requestJson = require('request-json');
var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechuigm/collections/";
var mLabAPIKey = "apiKey=YuQQuKMEavLfooSYMqUy0TK_zfKf_I00";
//fin  21_03_2018 incluimos líneas para consultar a mLab


app.listen(port);
console.log("API escuchando en el puerto " + port);


//
app.get ('/apitechu/v1',
     function(req,res) {
       console.log("GET /apitechu/v1");
       res.send(
         {
           "msg" : "Bienvenido a la API de TechU"
         }
       )
}
);

//

app.get('/apitechu/v1/users',
  function(req,res){
     console.log("GET /apitechu/v1/users");
     res.sendFile('login.json', {root: __dirname}); //enviamos login json como respuesta
  }
);

// 20_03_2018 version v2 para consultar usuarios a MongoDB --- localhost:3000/apitechu/v2/users
app.get('/apitechu/v2/users',
  function(req,res){
     console.log("GET /apitechu/v2/users");
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("ClienteCreado");
    httpClient.get("user?"+ mLabAPIKey,
       function(err, resMLab, body) {
         var response = !err ? body : {
           "msg" : "Error obteniendo usuarios."
         }
         res.send(response);
         }
       )
     }
);

// 20_03_2018 version v2 para consultar usuarios a MongoDB y hacer la consulta por id -- localhost:3000/apitechu/v2/users/2
app.get('/apitechu/v2/users/:id',
  function(req,res){
     console.log("GET /apitechu/v2/users");
     var id = req.params.id;
     var query = 'q={"id": ' + id + '}';
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("ClienteCreado");
    httpClient.get("user?" + query + "&" + mLabAPIKey,
       function(err, resMLab, body) {
         var response = !err ? body : {
           "msg" : "Error obteniendo usuarios."
         }
         res.send(response);
         }
       )
     }
);

// 20_03_2018 version v3 con gestión de errores para consultar usuarios a MongoDB y hacer la consulta por id -- localhost:3000/apitechu/v2/users/200 -- devuelve 404
app.get('/apitechu/v3/users/:id',
  function(req,res){
     console.log("GET /apitechu/v3/users");
     var id = req.params.id;
     var query = 'q={"id": ' + id + '}';
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("ClienteCreado");
    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {

          if (err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
          } else {
            if (body.length > 0) {
              response = body[0];
            } else {
              response = {
                "msg" : "Usuario no encontrado."
              };
              res.status(404);
            }
          }
          res.send(response);
          }
       )
     }
);


app.get('/apitechu/v2/users/:id/account',
  function(req,res){
     console.log("GET /apitechu/v2/users/:id/account");
     var id = req.params.id;
     var query = 'q={"userid": ' + id + '}';
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Obteniendo cuenta");
    httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {

          if (err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
          } else {
            if (body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario no encontrado."
              };
              res.status(404);
            }
          }
          console.log(response);
          res.send(response);
          }
       )
     }
);

//método get para obtener los movimientos por id de accounts
app.get('/apitechu/v2/users/:id/movements',
  function(req,res){
     console.log("GET /apitechu/v2/users/:id/movements");
     var id = req.params.id;
     var query = 'q={"accountid": ' + id + ' }';
     console.log(query);
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("ClienteCreado");
    console.log(httpClient);
    httpClient.get("movements?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {

          if (err) {
            response = {
              "msg" : "Error obteniendo movimientos."
            }
            res.status(500);
          } else {
            if (body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "No hay movimientos."
              };
              res.status(404);
            }
          }
          res.send(response);
          }
       )
     }
);

// 20_03_2018 version v3 con gestión de errores para consultar usuarios a MongoDB y hacer la consulta por id -- localhost:3000/apitechu/v2/users/200 -- devuelve 404
app.post('/apitechu/v2/login/',
  function(req,res){
     console.log("POST /apitechu/v2/login");
     console.log(req.body.email);
     console.log(req.body.password);


     var query = 'q={"email": "' + req.body.email + '", "password": "' + req.body.password + '"}';

     console.log(query);

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("ClienteCreado");

   httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {

          if (err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
          } else {
            if (body.length > 0) {
              response = body[0];
              var query = 'q={"id" : ' + body[0].id +'}';
              console.log(query);
              var putBody = '{"$set":{"logged":true}}';
              console.log(putBody);
              console.log(baseMlabURL + "user?" + query + "&" + mLabAPIKey, JSON.parse(putBody));

              httpClient.put ("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                 function(errPUT, resMLabPUT, bodyPUT){
                   if (errPUT) {
                     response = {
                       "msg" : "Error insertando usuario."
                     }
                   }else {
                   console.log("He realizado put");
                 }
               }
               );



            } else {
              response = {
                "msg" : "Usuario no encontrado."
              };
              res.status(404);
            }
          }
          console.log(response);
          res.send(response);
          }
       )
}
);

app.post('/apitechu/v2/logout/',
  function(req,res){
     console.log("POST /apitechu/v2/logout");
     console.log(req.body.userid);

     

    var query = 'q={"id": ' + req.body.userid + '}';

     console.log(query);

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("ClienteCreado");

   httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {

          if (err) {
            response = {
              "msg" : "Error obteniendo usuario."
            }
            res.status(500);
          } else {
            if (body.length > 0) {
              response = body[0];
              var query = 'q={"id" : ' + body[0].id +'}';
              console.log(query);
              var putBody = '{"$unset":{"logged":true}}';
              console.log(putBody);
              console.log(baseMlabURL + "user?" + query + "&" + mLabAPIKey, JSON.parse(putBody));

              httpClient.put ("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                 function(errPUT, resMLabPUT, bodyPUT){
                   if (errPUT) {
                     response = {
                       "msg" : "Error insertando usuario."
                     }
                   }else {
                   console.log("He realizado put");
                 }
               }
               );



            } else {
              response = {
                "msg" : "Usuario no encontrado."
              };
              res.status(404);
            }
          }
          console.log(response);
          res.send(response);
          }
       )
}
);

app.post('/apitechu/movements/insertmovement',
  function(req,res){
     console.log("POST /apitechu/movements/insertmovement/");
     console.log(req.body.id);
     console.log(req.body.date);
     console.log(req.body.amount);
     console.log(req.body.type);
     var newMovement = {
     "accountid" : req.body.id,
     "date" : req.body.date,
     "amount" : req.body.amount,
     "type" : req.body.type
   }

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente baseMlab Creado");
    console.log(newMovement.accountid);
    var query = 'q={"accountid": ' + newMovement.accountid + ' }';
    console.log(query);

    httpClient.get ("account?" + query + "&" + mLabAPIKey,
       function (err, resMLabPUT, body){
         if (err) {
            response = {
               "msg" : "Error recuperando la cuenta para insertar el movimientos"
             }
             res.status(500);
           }else {
             if (body.length > 0) {
             console.log(body);
             var query = 'q={"accountid" : ' + body[0].accountid +'}';
             console.log(query);
             var saldo = body[0].balance + Number(newMovement.amount);
             console.log(saldo);
             var putBody = '{"$set":{"balance": '+ saldo +'}}';
             console.log(putBody);
             console.log(baseMlabURL + "account?" + query + "&" + mLabAPIKey, JSON.parse(putBody));
             httpClient.put ("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT){
                  if (errPUT) {
                    response = {
                      "msg" : "Error actualizando saldo."
                    }
                  }else {
                      console.log(newMovement);
                      httpClient.post ("movements?" + mLabAPIKey, newMovement,
                          function(errPUT, resMLabPUT, bodyPUT){
                            if (errPUT) {
                              response = {
                               "msg" : "Error insertando movimientos."
                             }
                             res.status(500);
                             }else {
                              console.log(bodyPUT);
                              console.log(bodyPUT.accountid);
                              bodyPUT.balance = saldo;
                              console.log(bodyPUT.balance);
                              console.log("He insertado movimiento");
                              response = bodyPUT
                             }
                      console.log(response);
                      res.send(response);
                    }
                    );



                }
              }
              );

             }

           }
         }
       );
       }
);

app.post('/apitechu/v2/users/insertcustomer',
  function(req,res){
     console.log("POST /apitechu/v2/insertcustomer");
     console.log(req.body.id);
     console.log(req.body.first_name);
     console.log(req.body.last_name);
     console.log(req.body.email);
     console.log(req.body.password);

   httpClient = requestJson.createClient(baseMlabURL);
   console.log("Cliente baseMlab Creado");

   httpClient.get ("user?" + mLabAPIKey,
      function(errPUT, resMLabPUT, bodyPUT){
        if (errPUT) {
          response = {
            "msg" : "Error accediendo a la colección users."
          }
          res.status(500);
        }else {
            console.log(bodyPUT);

            var newCustomer = {
                "id" : bodyPUT.length + 1,
                "first_name" : req.body.first_name,
                "last_name" : req.body.last_name,
                "email" : req.body.email,
                "password": req.body.password,
                "logged": true
                }
            console.log("Antes de insertar el nuevo cliente");
            console.log(newCustomer);
            httpClient.post ("user?" + mLabAPIKey, newCustomer,
                 function(errPUT, resMLabPUT, bodyPUT){
                   if (errPUT) {
                     response = {
                       "msg" : "Error insertando usuario."
                    }
                    res.status(500);
                   }else {
                     console.log("Nuevo usuario insertado");
                   }
               }
             );
           }

         res.send(newCustomer);
         }


);
}
);

app.post('/apitechu/v2/accounts/insertaccount',
  function(req,res){
     console.log("POST /apitechu/v2/accounts/insertaccount");
     console.log(req.body.userid);
     console.log(req.body.iban);
     console.log(req.body.balance);


   httpClient = requestJson.createClient(baseMlabURL);
   console.log("Cliente baseMlab Creado");

   httpClient.get ("account?" + mLabAPIKey,
      function(errPUT, resMLabPUT, bodyPUT){
        if (errPUT) {
          response = {
            "msg" : "Error accediendo a la colección account."
          }
          res.status(500);
        }else {
            console.log(bodyPUT);

            var newAccount = {
                "accountid" : bodyPUT.length + 1,
                "userid" : req.body.userid,
                "balance" : req.body.balance,
                "iban" : req.body.iban
                }
            console.log("Antes de insertar la nueva cuenta");
            console.log(newAccount);
            httpClient.post ("account?" + mLabAPIKey, newAccount,
                 function(errPUT, resMLabPUT, bodyPUT){
                   if (errPUT) {
                     response = {
                       "msg" : "Error insertando cuentas."
                    }
                    res.status(500);
                   }else {
                     console.log("Nueva cuenta insertada");
                   }
               }
             );
           }

         res.send(newAccount);
         }


);
}
);


//definimos metodo para crear usuarios parametros se pasan por req.headers
app.post('/apitechu/v1/users',
  function(req,res){
    console.log("POST /apitechu/v1/users");
//    console.log(req);
//de momento, los pasamos por la cabecera
    console.log ("first_name es: " + req.headers.first_name);
    console.log ("last_name es: " + req.headers.last_name);
    console.log ("country es: " + req.headers.country);

//construimos un nuevo objeto JSON
    var newUser = {
      "first_name" : req.headers.first_name,
      "last_name" : req.headers.last_name,
      "country" : req.headers.country
    };

   var users = require('./usuarios.json');
   users.push(newUser);
   writeUserDataToFile(users);


   console.log("Usuario añadido con exito");



  }
);

//ejemplo de prueba de parametros

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)


// creo un nuevo post que recupera la información del body
app.post('/apitechu/v1/users/body',
  function(req,res){
    console.log("POST /apitechu/v1/users");
//    console.log(req);
//de momento, los pasamos por la cabecera
    console.log ("first_name es: " + req.body.first_name);
    console.log ("last_name es: " + req.body.last_name);
    console.log ("country es: " + req.body.country);

//construimos un nuevo objeto JSON
    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

   var users = require('./usuarios.json');
   users.push(newUser);
   writeUserDataToFile(users);


   console.log("Usuario añadido con exito");



  }
);



//incorporamos funcion para borrar incluyendo parameteos en la URL
app.delete('/apitechu/v1/users/:id',
  function(req,res){
    console.log("DELETE /apitechu/v1/users/:id");
    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);
    writeUserDataToFile(users);

    res.send(
      {
        "msg" : "Usuario borrado con exito"
      }
    )
  }
);


// función escritura de ficheros, hemos creado la funcion de
// escritura para factorizar la escritura de ficheros

function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  console.log ("Escribo fichero");

  fs.writeFile(
    "./login.json",
    jsonUserData,
    "utf8",
    function(err) {
      if(err) {
        console.log(err);
      } else {
        console.log("Usuario persistido");
      }
   }
 )
}


// LOGIN post para recuperar datos de LOGIN

app.post('/apitechu/v1/login',
  function(req,res){
    console.log("POST /apitechu/v1/login");
//    console.log(req);
//de momento, los pasamos por la cabecera


//construimos un nuevo objeto JSON

   var users = require('./login.json');


   for (user of users) {
      console.log (user.email);
      console.log (user.password);
      console.log("Paso por bucle");
    if (req.body.email == user.email) {
      if (req.body.password == user.password){
        console.log (user.email);
        console.log (user.password);
        console.log ("usuario encontrado");
        console.log ("Paso por la funcion");
        user.logged = true;

        writeUserDataToFile(users);

        res.send (
          {
            "msg": "Login correcto",
            "id" : user.id
          }
        )
      }
      break;
    }

}
res.send (
  {
    "msg" : "login incorrecto"
  }
)
console.log ("usuario no encontrado");
}
);

app.post('/apitechu/v1/logout',
  function(req,res){
    console.log("POST /apitechu/v1/logout");
//    console.log(req);
//de momento, los pasamos por la cabecera


//construimos un nuevo objeto JSON

   var users = require('./login.json');

   for (user of users) {

      console.log("Paso por bucle");
    if (req.body.id == user.id) {
      if (user.logged){
        console.log ("usuario encontrado, listo para borrar")
        delete user.logged;
        writeUserDataToFile(users);







        res.send(
          {
            "msg" : "Logout satisfactorio",
            "id" : user.id
          }
        )
      }
      break;
    }


   }

   res.send (
     {
       "msg" : "logout incorrecto"
     }
)


  }
);
