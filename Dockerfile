# Imagen raíz
FROM node

# Carpeta raíz. En la imagen de node, el directorio de trabajo en node va a haber un apitechu
WORKDIR /apitechu

# Copia de archivos. Lo que quiero añadir a la imagen es lo que está en el directorio de trabajo y destino a apitechu
ADD . /apitechu

#Añadir volume... definimos carpeta física en el contenedor. La imagen que estoy creando cuando se haga contenedor necesita un volumen
VOLUME ['/logs']

# Exponer puerto (el mismo que tenemos definido en el server.js)
EXPOSE 3000

# Comando de incialización (para arrancar la api)

CMD ["npm", "start"]
