var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

var server = require('../server.js');






// Prueba inicial
describe('First test',
  function() {
    it('Tests that DuckDuckGo works', function(done) {
      chai.request('http://www.duckducgo.com')
      // para probar expected & should
      // chai.request('http://www.elpais.es/ausiouaiosaioj')
      .get('/')
      .end (
        function (err, res) {
          console.log("Request has ended");
          // console.log(res);
          console.log(err);
          res.should.have.status(200); // el objeto res, debe tener una propiedad status = 200, si no tiene 200, no funciona
          done();
        }
      );
    });
 }
 );

// Prueba de nuestra API

 describe('Test de API Usuarios',

  function() {

    it('Prueba que la API de usuarios responde correctamente.',

      function(done) {

        chai.request('http://localhost:3000')

          .get('/apitechu/v1')

          .end(

            function(err, res) {

              res.should.have.status(200);

              res.body.msg.should.be.eql("Bienvenido a la API de TechU")

              done();

            }

          )

      }

    )

  }

 );

 // Prueba de nuestra API que devuelve nuestra lista de usuarios

  describe('Test de API Usuarios GET',

   function() {

     it('Prueba que la API devuelve una lista de usuarios correcta',

       function(done) {

         chai.request('http://localhost:3000')

           .get('/apitechu/v1/users')

           .end(

             function(err, res) {

               res.should.have.status(200);

           res.body.should.be.a("array");

           for (user of res.body) {

             user.should.have.property('email');

             user.should.have.property('password');
           }

               done();

             }

           )

       }

     )

   }

  );
